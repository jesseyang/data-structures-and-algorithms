package cn.jesseyang.java.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Client {
    public static void main(String[] args) {
        Car bmw = new BMW();
        InvocationHandler invocationHandler = new CarDynamicProxy(bmw);
        Car proxyBmw = (Car) Proxy.newProxyInstance(bmw.getClass().getClassLoader(), bmw.getClass().getInterfaces(), invocationHandler);

        proxyBmw.run();
        proxyBmw.didi();

    }
}
