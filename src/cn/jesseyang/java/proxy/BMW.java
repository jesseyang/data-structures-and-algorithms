package cn.jesseyang.java.proxy;

public class BMW implements Car {
    @Override
    public void run() {
        System.out.println("bmw run in the road");
    }

    @Override
    public void didi() {
        System.out.println("bmw didididididi");
    }
}
