package cn.jesseyang.java.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CarDynamicProxy implements InvocationHandler {

    private Object subject;

    public CarDynamicProxy(Car car) {
        this.subject = car;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("whatever car want to do ,run me first");
        method.invoke(subject, args);
        System.out.println("after car do sth");
        return null;
    }
}
