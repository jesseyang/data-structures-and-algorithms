package cn.jesseyang.leetcode.sort;

public class OddEvenSort {
    public int[] sortArrayByParityII(int[] a) {
        int l = a.length;
        int[] oddArray = new int[l / 2];
        int[] evenArray = new int[l / 2];
        int[] result = new int[l];
        int oddIndex = 0, evenIndex = 0;
        for (int i = 0; i < l; i++) {
            if (a[i] % 2 == 0) {
                evenArray[evenIndex] = a[i];
                evenIndex++;
            } else {
                oddArray[oddIndex] = a[i];
                oddIndex++;
            }
        }
        oddIndex = 0;
        evenIndex = 0;
        for (int i = 0; i < l; i++) {
            if (i % 2 == 0) {
                result[i] = evenArray[evenIndex];
                evenIndex++;
            } else {
                result[i] = oddArray[oddIndex];
                oddIndex++;
            }
        }


        return result;
    }
}
