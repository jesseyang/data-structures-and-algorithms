package cn.jesseyang.graph;

import java.util.*;

/*
    有向图的广度搜索思路

 */
public class Graph {
    private Map<String, LinkedList<String>> adjList; //节点名称是key，value是节点的邻接表

    public Graph() {
        this.adjList = new HashMap<>();
    }

    public void init() {
        //初始化图
        LinkedList<String> adj1 = new LinkedList<>();
        adj1.add("2");
        adj1.add("3");
        LinkedList<String> adj2 = new LinkedList<>();
        adj2.add("4");
        adj2.add("5");
        LinkedList<String> adj3 = new LinkedList<>();
        adj3.add("4");
        adj3.add("2");
        adj3.add("6");
        LinkedList<String> adj4 = new LinkedList<>();
        adj4.add("5");
        LinkedList<String> adj5 = new LinkedList<>();
        adj5.add("6");
        LinkedList<String> adj6 = new LinkedList<>();
        this.adjList.put("1", adj1);
        this.adjList.put("2", adj2);
        this.adjList.put("3", adj3);
        this.adjList.put("4", adj4);
        this.adjList.put("5", adj5);
        this.adjList.put("6", adj6);
    }

    public void bfs(String startName, String endName) {
        Map<String, String> lastNodeMap = new HashMap<>();//用来记录路径 key是节点，value是上一个节点

        Queue<String> waitSearchQueue = new LinkedList<>();
        List<String> visitedList = new ArrayList<>(); //已经访问过的，防止重复访问
        waitSearchQueue.offer(startName);
        while (!waitSearchQueue.isEmpty()) {
            //从等待搜索队列第一个弹出一个
            String currentNode = waitSearchQueue.poll();
            //如果没访问过
            if (visitedList.indexOf(currentNode) < 0) {
                System.out.println("当前搜索节点： " + currentNode);
                //如果就是目标 停止
                if (currentNode.equals(endName)) {
                    System.out.println("找到节点：" + endName);
                    printPath(currentNode, lastNodeMap);
                    return;
                } else {
                    LinkedList<String> currentAdjList = this.adjList.get(currentNode);
                    for (int i = 0; i < currentAdjList.size(); i++) {
                        if (!waitSearchQueue.contains(currentAdjList.get(i)) && visitedList.indexOf(currentAdjList.get(i)) < 0) {
                            //加入等待搜索队列
                            waitSearchQueue.offer(currentAdjList.get(i));
                            //记录节点的上一个节点（currentnode)
                            lastNodeMap.put(currentAdjList.get(i), currentNode);
                        }

                    }
                }
                visitedList.add(currentNode);
            }
        }
        System.out.println("未找到路径");
    }

    private void printPath(String name, Map<String, String> lastNodeMap) {
        String lastNode = lastNodeMap.get(name);
        List<String> pathList = new ArrayList<>();
        pathList.add(name);
        while (lastNode != null) {
            pathList.add(lastNode);
            lastNode = lastNodeMap.get(lastNode);
        }
        for (int i = pathList.size() - 1; i >= 0; i--) {
            System.out.print(pathList.get(i));
            if (i > 0) System.out.print("=>");
        }
    }

    public static void main(String[] args) {
        System.out.println("有向图的广度搜索");

        Graph graph = new Graph();
        graph.init();
        graph.bfs("1", "6");
    }


}

