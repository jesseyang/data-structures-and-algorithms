package cn.jesseyang.sort;

/**
 * @Auther: JesseYang
 * @Date: 2018/12/4 20:45
 * @Description:
 */
public class QuickSort {


    public static int findPosition(int[] a, int left, int right) {
        int current = a[left];
        while (left < right) {
            while (left < right && a[right] > current) {
                right--;
            }
            //碰到小的移到左边
            a[left] = a[right];
            while (left < right && a[left] < current) {
                left++;
            }
            //碰到大的移到右边
            a[right] = a[left];
        }
        a[left] = current;
        return left;
    }

    public static void quickSort(int[] a, int left, int right) {
        if (left >= right) {
            return;
        }
        int mid = findPosition(a, left, right);
        quickSort(a, left, mid);
        quickSort(a, mid + 1, right);

    }


    public static void main(String[] args) {
        int[] a = {234, 2123, 124, 1243, 5, 45645, 57};
        quickSort(a, 0, a.length - 1);
        for (int b :
                a) {
            System.out.println(" " + b);
        }
    }

}
