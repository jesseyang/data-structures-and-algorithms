package cn.jesseyang.sort;

/**
 * @Auther: JesseYang
 * @Date: 2018/12/2 20:07
 * @Description: 归并排序 先二分成单个的数，然后合并 利用递归完成
 */
public class MergeSort {

    public static void mergeSort(int[] a,int start,int end){
        if(start>=end){
            return;
        }

        int mid  = (start+end)/2;
        mergeSort(a,start,mid);
        mergeSort(a,mid+1,end);
        merge(a,start,mid,end);
    }

    private static void merge(int[] a, int start, int mid, int end) {
        int[] temp = new int[a.length];
        int p1 = start; // 左边数组的位置指针
        int p2 = mid+1; // 右边数组的位置指针
        int p  = start; // 临时数组的位置指针
        while( p1<=mid && p2<=end ){
            if(a[p1]<a[p2]){
                temp[p++] = a[p1++];
            }else{
                temp[p++] = a[p2++];
            }
        }

        while(p1<=mid)temp[p++] = a[p1++];
        while(p2<=end)temp[p++] = a[p2++];

        for (int i = start; i <= end; i++) {
            a[i]=temp[i];
        }

    }

    public static void main(String[] args) {
        int[] a = {234,2123,124,1243,5,45645,57};
        mergeSort(a,0,a.length-1);
        for (int b:
                a) {
            System.out.println(" " + b);
        }
    }
}
