package cn.jesseyang.sort;

/**
 * @Auther: JesseYang
 * @Date: 2018/12/2 19:59
 * @Description: 插入排序， 将最左边第一个认为是一个排好序的队列，从第二个开始依次插入到左边的有序的队列里去
 */
public class InsertSort {
    public static void insertSort(int[] a){
        int n = a.length;
        for (int i = 1; i < n; i++) {
            //将要插进左边排好序数组里面去的数
            int current  = a[i];
            // j 是左边排好序数组的位置下标
            int j = i-1;
            for (; j >= 0; j--) {
                //如果比将要插入数组的数大，就往右移一个位置，给这个数腾出来地方
                if(a[j]>current){
                    a[j+1] = a[j];
                }else{
                    //如果不比他大，说明a[j+1]这个位置就应该是待比较数该待的位置
                    break;
                }

            }
            a[j+1] = current;
        }

    }

    public static void main(String[] args) {
        int[] a = {234,2123,124,1243,5,45645,57};
        insertSort(a);
        for (int b:
                a) {
            System.out.println(" " + b);
        }
    }
}
