package cn.jesseyang.sort;

/**
 * @Auther: JesseYang
 * @Date: 2018/12/4 20:54
 * @Description:
 */
public class CountingSort {
    public static void countingSort(int[] a) {
        int n = a.length;
        int max = 0;
        //首先找出a中最大的数
        for (int i = 0; i < n - 1; i++) {
            if (a[i + 1] > a[i]) {
                max = a[i + 1];
            }
        }
        //每一个数一个桶
        int[] c = new int[max + 1];
        //计算每个数有多少个
        for (int i = 0; i < n; i++) {
            c[a[i]]++;
        }

        //对c进行顺序累加
        for (int i = 0; i < c.length - 1; i++) {
            c[i + 1] = c[i] + c[i + 1];
        }

        //定义一个数组，去装排好序的a
        int[] r = new int[n];

        //c的对应index的值就是a数组的元素的index。每取一个就-1，因为有重复的
        for (int i = n - 1; i >= 0; i--) {
            int index = c[a[i]] - 1;
            r[index] = a[i];
            c[a[i]]--;
        }

        //放回a
        for (int i = 0; i < n; i++) {
            a[i] = r[i];
        }


    }

    public static void main(String[] args) {
        int[] a = {5, 4, 3, 4, 2, 1, 0, 6, 4, 4};
        countingSort(a);
        for (int b :
                a) {
            System.out.println(" " + b);
        }
    }
}
