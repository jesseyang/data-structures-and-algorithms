package cn.jesseyang.sort;

/**
 * @Auther: JesseYang
 * @Date: 2018/12/2 19:52
 * @Description: 冒泡排序 ，每次把最大的放到数组最后面
 */

public class BubbleSort {
    public static void bubbleSort(int[] a){
        int n = a.length;

        boolean changeFlag = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0 ; j <n-i-1 ; j++) {
                if(a[j]>a[j+1]){
                    int temp;
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                    changeFlag = false;
                }

            }
            //如果一个都没换，说明已经排好序了
            if(changeFlag){
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] a = {234,2123,124,1243,5,45645,57};
        bubbleSort(a);
        for (int b:
             a) {
            System.out.println(" " + b);
        }
    }
}
