class Node {
    constructor(data) {
        this.root = this;
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

class BinarySearchTree {
    constructor() {
        this.root = null
    }

    //二叉树插入
    insert(data) {
        const newNode = new Node(data)
        const insertNode = (node, newNode) =
    >
        {
            if (newNode.data < node.data) { //左
                if (node.left == null) {
                    node.left = newNode
                } else {
                    insertNode(node.left, newNode)
                }
            } else if (newNode.data > node.data) {
                if (node.right == null) {
                    node.right = newNode
                } else {
                    insertNode(node.right, newNode)
                }
            } else { //相等
                //todo
            }
        }
        if (this.root == null) {
            this.root = newNode
        } else {
            insertNode(this.root, newNode)
        }
    }

    //二叉树查找
    search(data) {
        if (this.root == null) {
            return null
        }
        const searchNode = (node, data) =
    >
        {
            if (node == null) return false
            if (node.data == data) {
                return node
            }
            return searchNode(node.data > data ? node.left : node.right, data)
        }
        return searchNode(this.root, data)
    }

    //前序遍历 根 左 右
    preOrder() {
        let result = []
        if (this.root == null) return result
        const inOrderNode = (node) =
    >
        {
            if (node != null) {
                result.push(node.data)
                inOrderNode(node.left)
                inOrderNode(node.right)
            }
        }
        inOrderNode(this.root)
        return result
    }

    //中序遍历 左 根 右
    midOrder() {
        let result = []
        if (this.root == null) return result
        const inOrderNode = (node) =
    >
        {
            if (node != null) {
                inOrderNode(node.left)
                result.push(node.data)
                inOrderNode(node.right)
            }
        }
        inOrderNode(this.root)
        return result
    }

    //后序遍历 左 右 根
    backOrder() {
        let result = []
        if (this.root == null) return result
        const inOrderNode = (node) =
    >
        {
            if (node != null) {
                inOrderNode(node.left)
                inOrderNode(node.right)
                result.push(node.data)
            }
        }
        inOrderNode(this.root)
        return result
    }


}

const tree = new BinarySearchTree();
tree.insert(7);
tree.insert(11);
tree.insert(5);
tree.insert(3);
tree.insert(9);
tree.insert(8);
tree.insert(10);
tree.insert(13);
tree.insert(12);
tree.insert(14);
tree.insert(20);
tree.insert(18);
tree.insert(25);
// console.log(tree.search(20))
console.log(tree.preOrder())
console.log(tree.midOrder())
console.log(tree.backOrder())